import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Appbar, TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
const EditAddress = ({navigation,route}) => {
    //const { itemId } = route.params 
    const { itemId } = route.params
    const { itemPincode } = route.params
    const {itemCity}   = route.params
    const {itemFn}    =  route.params
    const {itemLn}    = route.params
    const {itemAddress}   = route.params
    const {itemLandmark}    = route.params
    const {itemMobile}    = route.params

  const [id, setId] = useState(itemId);
  const [first_name, setFirstname] = useState(itemFn);
  const [last_name, setLastname] = useState(itemLn);
  const [pincode, setPincode] = useState(itemPincode);
  const [city_state, setCity] = useState(itemCity);
  const [address, setAddress] = useState(itemAddress);
  const [landmark, setLandmark] = useState(itemLandmark);
  const [mobile, setMobile] = useState(itemMobile);

  
    
  
    //   setFirstname(itemFn),
    //   setLastname(itemLn),
    //   setPincode(itemPincode),
    //   setCity(itemCity),
    //   setAddress(itemAddress),
    //   setLandmark(itemLandmark),
    //   setMobile(itemMobile)
  

//    useEffect( async() =>{
//     try {
//         let user = await AsyncStorage.getItem('userdetails');
//         user = JSON.parse(user);
//         setUserid(user.id)
//         //console.log('user id=>', user.id)
//         //console.log('userData =>', user);
//       } catch (error) {
//         console.log(error);
//       }
//   });

console.log('pincode--->',itemPincode);

let item1 = {first_name}
    console.log('item------>',item1);

const UpdateUser = () => {
    let item = {first_name,last_name,pincode,city_state,address,landmark,mobile}
    console.log('item',item);
    let url = 'http://3.12.158.241/medical/api/modifyAddress/'+id;
    console.log('URL===',url);
    fetch('http://3.12.158.241/medical/api/modifyAddress/'+id,{
      method: 'PUT',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(item)
  })
  .then(response => response.json())
  .then(data => {
      console.log(data);
      alert('Address Updated Successfully')
  })
  .catch(err => console.error(err));
}

  

return(
    
  <ScrollView style={styles.contentBody}>
  
  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <TextInput
          label= {itemPincode}
          value={JSON.stringify(pincode)}
          onChangeText={(inputText) => setPincode(inputText)}
          style={[styles.textInputall, styles.widtthirty]}
          />
      <TextInput
          //label={itemCity}
          value={city_state}
          onChangeText={(inputText) => setCity(inputText)}
          style={[styles.textInputall, styles.widtsuxtysix]}
          />
  </View>
  <Text/>
  <Text/>
  <TextInput
      //label={itemFn}
      value={first_name}
      onChangeText={(inputText)=>setFirstname(inputText)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      //label={itemLn}
      value={last_name}
      onChangeText={(inputText) => setLastname(inputText)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      //label={itemAddress}
      value={address}
      onChangeText={(inputText) => setAddress(inputText)}
      multiline = {true}
      numberOfLines = {5}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      //label={itemLandmark}
      value={landmark}
      onChangeText={(inputText) => setLandmark(inputText)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      //label={itemMobile}
      value={JSON.stringify(mobile)}
      onChangeText={(inputText) => setMobile(inputText)}
      style={styles.textInputall}
      />
  
  <Text/>
  <View style={{alignItems: 'center'}}>
  <TouchableOpacity onPress={UpdateUser} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Update</Text>
      </TouchableOpacity>
  </View>
  <Text/>
  </ScrollView>
);
}
export default EditAddress;

const styles = StyleSheet.create({
  image: {
      width: '100%'
  },
  contentBody: {
      paddingHorizontal: 15,
      paddingTop:10,
  },
  textInputall: {
      backgroundColor: '#fff'
  },
  widtthirty: {
      width: '30%',
  },
  widtfortyet: {
      width: '48%',
  },
  radios: {
      flexDirection: 'row',
      justifyContent: 'space-evenly'
  },
  widtsuxtysix: {
      width: '66%',
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 60,
    width: '50%',
    marginLeft: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
});