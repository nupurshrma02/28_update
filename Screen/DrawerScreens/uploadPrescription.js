import React, {Component} from 'react';
import {
    View, Text, StyleSheet, ScrollView, Alert,
   Image, TouchableOpacity, NativeModules, Dimensions, StatusBar, SafeAreaView,Modal
  ,Button, LayoutAnimation, UIManager, Platform} from 'react-native';
  import {Picker} from '@react-native-picker/picker';
//import {CarColors} from "../assets/Colors";
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';

//var commonStyles = require('../assets/style');
var ImagePicker = NativeModules.ImageCropPicker;
let data = new FormData();

export default class UploadPrescription extends Component {

    constructor() {
        super();

        if( Platform.OS === 'android' )
        {

          UIManager.setLayoutAnimationEnabledExperimental(true);

        }
        this.state = {
            image: null,
            images: null,
            isVisible: false,
            userid: '',
            
            
           textLayoutHeight: 0,
           updatedHeight: 0, 
           expand: false,
           Icon : 'chevron-down'
        };
        // state = {  
        //     isVisible: false, //state of modal default false  
        //   }
    }

    componentDidMount() {
        this.checkingAsync();
      }

      checkingAsync = async () => {
        try {
          let user = await AsyncStorage.getItem('userdetails');
          user = JSON.parse(user);
          this.setState({userid : user.id})
          console.log('user id=>', user.id)
          console.log('userData =>', user);
        } catch (error) {
          console.log(error);
        }
      };

    expand_collapse_Function =()=>
    {
        LayoutAnimation.configureNext( LayoutAnimation.Presets.easeInEaseOut );
 
        if( this.state.expand == false )
        {
            this.setState({ 
              updatedHeight: this.state.textLayoutHeight, 
              expand: true, 
              Icon: 'chevron-up' 
            }); 
        }
        else
        {
            this.setState({ 
              updatedHeight: 0, 
              expand: false, 
              Icon: 'chevron-down' 
            });
        }
    }
 
    getHeight(height)
    {
        this.setState({ textLayoutHeight: height });
    }


    cleanupImages() {
        ImagePicker.clean().then(() => {
            // console.log('removed tmp images from tmp directory');
            alert('Temporary images history cleared')
        }).catch(e => {
            alert(e);
        });
    }

    pickMultiple= () => {
        ImagePicker.openPicker({
            multiple : true,
            waitAnimationEnd: false,
            includeExif: true,
            forceJpg: true,
            maxfiles: 5,
            compressImageQuality: 0.8,
            meidiaType: 'photo'
        }).then(images => {
            console.log('imagesss-->',images);
            images.forEach((item,index)=> {
                console.log('received image', item);
                data.append("image[]", {
                    uri: item.path,
                    type: "image/jpeg",
                    //correctFilename: item.filename||`temp_image_${index}.jpg`,
                    name: item.filename||`temp_image_${index}.jpeg`,
                    })
                    //return {uri: item.path, width: item.width, height: item.height, mime: item.mime};
                    })
                   }).catch(e => alert(e));
                }

    upload = () => {
        
        console.log('Upload---->',JSON.stringify(data));
          let User_id = this.state.userid;
          let dataFetchUrl = 'http://3.12.158.241/medical/api/image/'+ User_id;
          console.log("url----->>>>", dataFetchUrl);
          fetch(dataFetchUrl,{
              method: "post",
              headers: {
                  'Accept' : "application/x-www-form-urlencoded",
                  'Content-Type': 'application/json'
              },      
              body: JSON.stringify(data),
        
      }).then((res) => res.json())
      .then(resData => {
          console.log(resData);
          console.log('success');
          alert("Success")
      }).catch(err => {
         console.error("error uploading images:", err);
         console.log('datttttaa--->', JSON.stringify(data));
      })
      }

    scaledHeight(oldW, oldH, newW) {
        return (oldH / oldW) * newW;
    }

    renderImage(image) {
        return (
        <View style={{flexDirection: 'row'}}>
            <Image style={{width: 100, height: 100, resizeMode: 'contain',}} source4={image}/>
        </View>
        );
    }

    renderAsset(image) {
        if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
            return this.renderVideo(image);
        }

        return this.renderImage(image);
    }

    render() {
        return (
            <SafeAreaView style={styles.safeArea}>
                 <Text style={{fontWeight: 'bold', marginLeft: 20, fontSize: 20,}}>Have a Prescription? Upload here</Text>
                <View style={styles.container1}>
                <Modal            
                 animationType = {"fade"}  
                 transparent = {false}  
                 visible = {this.state.isVisible}  
                 onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
                {/*All views of Modal*/}  
                 <View style = {styles.modal}>
                 <TouchableOpacity onPress = {() => {  
                  this.setState({ isVisible:!this.state.isVisible})}}>  
                 <Icon name="times" size={25} color='#fff' style={{marginLeft:300,}}/>
                 </TouchableOpacity>
                 <Text style = {styles.text}>Valid Prescription Guide</Text>
                 <View style={{alignItems: 'center'}}>
                 <Image source = {require('../../Image/receipt.jpeg')} 
                 style={{height: 370, width: 300,}} />
                 </View>
                 <Text style={{color: '#fff', marginLeft:10}}>
                     include details of doctor and patient + {'\n'} clinic visit details
                </Text>
                <Text style={{color: '#fff', marginLeft:10}}>
                     Medicines Will be dispensed as per {'\n'} Prescription
                </Text>  
                   
                </View>  
                </Modal>  
                
                <TouchableOpacity onPress={this.upload}>
                <Icon name="camera" size={25} color='#fff' style={{marginLeft:20}}/>
                <Text style={{color:'#fff', marginLeft: 10}}>Camera</Text>
                </TouchableOpacity>
                <View style={styles.verticleLine}></View>
                <TouchableOpacity onPress={this.pickMultiple.bind(this)}>
                <Icon name="image" size={30} color='#fff' style={{marginLeft:40, marginRight: 20,}}/>
                <Text style={{color:'#fff', marginLeft: 40}}>Gallery</Text>
                </TouchableOpacity>
                <View style={styles.verticleLine}></View>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('PrescriptionStack')}>
                <Icon name="file" size={25} color='#fff' style={{marginLeft:40,}}/>
                <Text style={{color:'#fff', marginLeft: 20,}}>Prescription</Text>
                </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                <Icon name="info-circle" size={20} color='#1e90ff' style={{marginLeft:20, marginTop:20}}/>
                <TouchableOpacity onPress = {() => {this.setState({ isVisible: true})}}  >
                <Text style={{color:'#1e90ff', marginTop: 20, marginLeft: 10}}>Valid Prescription Guide</Text>
                </TouchableOpacity>
                </View>
                
                <View style = { styles.MainContainer }>
                <View style = { styles.ChildView }>
                  <Text style={{marginLeft: 20, marginTop: 10, color:'#1e90ff'}}>Why upload a Prescription?</Text>
                  <TouchableOpacity activeOpacity = { 0.7 } 
                  onPress = { this.expand_collapse_Function }>
                  <Icon name={this.state.Icon} size={20} color='#1e90ff' style={{marginLeft:20, marginTop:10,}}/>
                  </TouchableOpacity>
                  <View style = {{ height: this.state.updatedHeight, overflow: 'hidden' }}>

                  {/* {this.state.Icon == 'chevron-down'} ? */}
                        {/* <Text style = { styles.ExpandViewInsideText } 
                              onLayout = {( value ) => this.getHeight( value.nativeEvent.layout.height )}>
                            
                            Hello Developers, A warm welcome on ReactNativeCode.com, The best website for react native developers.
                            You can find high quality dynamic type of tutorials with examples on my website and to support us please like our Facebook page.

                        </Text> */}
                    
                    
                    </View>
                
                </View>
                </View>
        
                <ScrollView style={styles.imgContainer}>
                    {this.state.image ? this.renderAsset(this.state.image) : null}
                    {this.state.images ? this.state.images.map((item,index) => <View style={styles.imgView}
                                                                          key={item.uri}>{this.renderAsset(item)}</View>) : null}
                    {
                        this.state.images &&
                        <TouchableOpacity onPress={this.upload()} >
                            <Text>Upload</Text>
                        </TouchableOpacity>
                    }
                </ScrollView>


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    container1: {
        backgroundColor: '#1e90ff',
        padding: 10,
        elevation: 8,
        borderRadius: 50,
        width: '80%',
        //alignItems: 'center'
        marginLeft: 20,
        flexDirection: 'row',
        marginTop: 10,
    },
    imgContainer: {
        marginVertical: 40,
        //marginHorizontal: 40,
        flexDirection: 'row',
    },
    button: {
        backgroundColor: 'blue',
        marginBottom: 10,
    },
    // MainContainer:
    // {
    //     flex: 1,
    //     justifyContent: 'center',
    //     paddingTop: (Platform.OS === 'ios') ? 20 : 0
    // },

    ChildView:
    {
        //borderWidth: 1,
        //borderColor: '#00BCD4',
        margin: 5,
        flexDirection: 'row',
    },
    text: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 22
    },
    safeArea: {
        marginTop: 20,
    },
    dateContainer: {
        flexDirection: 'row',
    },
    imgView: {
        width: '50%',
        marginVertical: 10,
        flexDirection: 'row',
    },
    verticleLine: {
        height: 50,
        width: 1,
        backgroundColor: '#fff',
        marginLeft: 20
      },
      modal: {  
        //justifyContent: 'center',  
        //alignItems: 'center',   
        backgroundColor : "#1e90ff",   
        height: 500 ,  
        width: '80%',  
        borderRadius:10,  
        borderWidth: 1,  
        borderColor: '#fff',    
        marginTop: 80,  
        marginLeft: 40,  
         
         },  
         text: {  
            color: '#fff',  
            marginTop:-20,
            marginLeft: 10,
            fontSize: 20,  
            fontWeight: 'bold',
         },
    ExpandViewInsideText:
    {
        fontSize: 16,
        color: '#000',
        //padding: 12
    }
  
});