import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, FlatList, Alert, ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Address} from './Components/dummyData';
import AsyncStorage from '@react-native-community/async-storage';
import EditAddress from './DrawerScreens/EditAddress';

export default class DeliveryAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      type: '',
      data: [],
      selectedId: null,
      //selectedItem: null,
      status:true,
    };
    // this.setState({
    //   type: this.state.type
    // })
  }

 
ShowHideTextComponentView = () =>{
 
    if(this.state.status == false)
    {
      this.setState({status: true})
    }
    else
    {
      this.setState({status: false})
    }
  }

  handleSelection = (id) => {
    var selectedId = this.state.selectedId
    console.log('idddd--->', selectedId);
 
    if(selectedId === id)
      this.setState({selectedId: null})
    else 
      this.setState({selectedId: id})
 }

//  deleteItemById = id => {
//   const filteredData = this.state.data.filter(item => item.id !== id);
//   this.setState({ data: filteredData });
// }

UpdateData = (itemProp,pincode,city,fn,ln,address,landmark,mobile) => {
  this.props.navigation.navigate('EditAddress',{
    
    itemId: itemProp,
    itemPincode: pincode,
    itemCity: city,
    itemFn : fn,
    itemLn : ln,
    itemAddress: address,
    itemLandmark: landmark,
    itemMobile: mobile,
  });
  const itemid = pincode;
  console.log('iddddddddddddd======>>>>>',itemid);
}



deleteData = (id) => {
  fetch('http://3.12.158.241/medical/api/deleteAddress/'+id,{
      method: 'DELETE',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
      }
  })
  .then(response => response.json())
  .then(data => {
      console.log(data);
      alert('Address Deleted Successfully')
  })
  .catch(err => console.error(err));
} 

componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    //let user_id= setUserid(user.id);
    console.log('user id=>', user.id);
    let fetchUrl = 'http://3.12.158.241/medical/api/getUserDetails/'+user.id;
    console.log('deliveryAddressUrl------>', fetchUrl);
    const response = await fetch(fetchUrl);
    const json = await response.json();
    this.setState({data: json.data});
    var myDataArray = json.data;
    console.log('cgfgfg-->',myDataArray);
    myDataArray.length === 0 ? this.props.navigation.navigate('AddAdressScreen'): '';
  };

   renderItem = ({item}) => {
     
    return(
      <View>
      <TouchableOpacity activeOpacity={.7} onPress={() => this.handleSelection(item.id)}
      style={item.id === this.state.selectedId ? styles.selected : null}>
          <View style={styles.addressContainer}>
          {/* <Icon name="check-circle" size={25}  style={styles.checkIcon}/> */}
          {item.id === this.state.selectedId ? <Icon name="check-circle" size={25}  style={styles.checkIcon}/>
          :<Icon name="circle" size={25}  style={styles.unCheckIcon}/>}
           <Text style={styles.textStyleName}>{item.first_name} {item.last_name}</Text> 
           <View><Text style={{position:'absolute', marginLeft: -30, fontSize: 15,}}>
             {item.address} {item.city_state}, {"\n"}
             {item.city_state}-{item.pincode} {"\n"}
             mobile no: {item.mobile}</Text></View>
           <View>
             {
          // Pass any View or Component inside the curly bracket.
          // Here the ? Question Mark represent the ternary operator.
          this.state.status == false && this.state.selectedId==item.id ? <Text style= {styles.addressType}>Primary Address</Text> : null
           }

            </View>
            </View> 
             </TouchableOpacity>

             {this.state.selectedId === item.id ? 
             <View style={{ padding: 20, backgroundColor:'#fff',marginLeft: 8,
                          marginRight: 8, alignItems: 'center', }}>
             <TouchableOpacity style={styles.modifyButton1} onPress={() => this.UpdateData(item.id,
              item.pincode,item.city_state,item.first_name,item.last_name,item.address,
              item.landmark,item.mobile)}>
             <Text style={styles.modifyButtonText} activeOpacity={.7}>Modify</Text>
             </TouchableOpacity>
             </View> :
             <View style={{ padding: 20, backgroundColor:'#fff',marginLeft: 8,
                           marginRight: 8, flexDirection: 'row', justifyContent:'space-around',}}>
             <TouchableOpacity style={styles.modifyButton} onPress={() => this.UpdateData(item.id,
              item.pincode,item.city_state,item.first_name,item.last_name,item.address,
              item.landmark,item.mobile)}>
             <Text style={styles.modifyButtonText} activeOpacity={.7}>Modify</Text>
             </TouchableOpacity>
             <TouchableOpacity style={styles.modifyButton} onPress={() => this.deleteData(item.id)}>
             <Text style={styles.modifyButtonText} activeOpacity={.7}>Delete</Text>
             </TouchableOpacity>
             </View>
             }
             {/* {
             this.state.selectedId === item.id ?
             <View style={{position:'absolute'}}>
             <TouchableOpacity activeOpacity={.7} onPress= {Alert.alert('modified')}style={styles.modifyButton}>
               <Text style={styles.modifyButtonText}>Modify</Text>
             </TouchableOpacity></View>: <View style={{position:'absolute', flexDirection:'row'}}>
             <TouchableOpacity style={styles.modifyButton}>
             <Text style={styles.modifyButtonText} activeOpacity={.7}>Modify</Text>
             </TouchableOpacity>
             <TouchableOpacity style={styles.modifyButton}>
             <Text style={styles.modifyButtonText} activeOpacity={.7}>Delete</Text>
            </TouchableOpacity>
            </View>
             } */}
             
            </View>
    );
 }

 

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{backgroundColor: '#fff', padding: 10, marginTop: 20, alignItems:'center'}}>
        <TouchableOpacity onPress={()=> this.props.navigation.navigate('AddAdressScreen')} 
        style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Add Address</Text>
         {/* <Text>{this.state.type}</Text> */}
        </TouchableOpacity>
        </View>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          keyExtractor={(item,index) => index.toString()}
          numColumns={1}
          extraData={this.state.selectedId,this.state.selectedtype}

        />
          {this.state.selectedId &&
          <View style={{backgroundColor: '#fff', padding: 10, paddingTop:10, marginTop: -10, alignItems:'center'}}>
          <TouchableOpacity onPress={this.ShowHideTextComponentView} 
          style={styles.appButtonContainer1}>
           <Text style={styles.appButtonText}>Select Delivery Preference</Text>
          </TouchableOpacity>
          </View>}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
       // backgroundColor: '#a9a9a9',
    },
    addressType:{
      position: 'absolute',
      marginTop: 65,
      fontSize: 15,
      marginLeft: -50,
      color:'#1e90ff',
      backgroundColor: '#87cefa',
      padding: 10,
      elevation:8,
      borderRadius: 10,
    },
    modifyButton:{
      //marginTop: 130,
      //marginLeft: 100,
      elevation: 8,
      backgroundColor: "#dcdcdc",
      borderRadius: 10,
      paddingVertical: 5,
      paddingHorizontal: 40,
      paddingLeft: 80,
      //paddingRight: 80,
      //marginTop: 20,
      width: '10%',
      height: 40,
      //marginLeft: 20

    },  
    modifyButton1:{
      //marginTop: 130,
      //marginLeft: 100,
      elevation: 8,
      backgroundColor: "#dcdcdc",
      borderRadius: 10,
      paddingVertical: 5,
      //paddingHorizontal: 40,
      paddingLeft: 80,
      //paddingRight: 80,
      //marginTop: 20,
      width: 300,
      height: 40,
      //marginLeft: 20

    },
    selected:{
      backgroundColor:'#fff', 
      //padding: 100, 
      marginTop:10,
      borderColor: '#1e90ff', 
      borderWidth:1,
      borderRadius: 10, 
      elevation:8,
      marginLeft: 8,
      marginRight: 8,
    },
    modifyButtonText:{
      fontSize: 20,
      color: "#000",
      fontWeight: "bold",
      alignSelf: "center",
      textTransform: "uppercase",
      position: 'absolute',
      marginTop: 10,
    },
    textStyleName:{
      fontWeight: 'bold',
      fontSize: 19,
      marginTop:-65,
      marginLeft: -30,
    },
    checkIcon: {
      position: 'absolute',
      marginTop: 20,
      marginLeft: 10,
      color:'#1e90ff',
    },
    unCheckIcon:{
      position: 'absolute',
      marginTop: 20,
      marginLeft: 10,
      color:'#dcdcdc',
    },
    addressContainer:{
      backgroundColor:'#fff', 
      padding: 80, 
      marginTop:10,
      borderColor: '#dcdcdc', 
      borderWidth:1,
      //borderRadius: 10, 
      elevation:8,
      marginLeft: 8,
      marginRight: 8,
    },
    appButtonContainer: {
        elevation: 8,
        backgroundColor: "#1e90ff",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 30,
        //marginTop: 60,
        width: '50%',
        //marginLeft: 50
      },
      appButtonContainer1:{
        elevation: 8,
        backgroundColor: "#1e90ff",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 30,
        marginTop: 20,
        width: '100%',
        //marginLeft: 50
      },
      appButtonText: {
        fontSize: 18,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
      },
})